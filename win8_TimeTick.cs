
public sealed partial class BlankPage : Page 
{ 
	public BlankPage() 
	{ 
		this.InitializeComponent(); 
		DispatcherTimer timer = new DispatcherTimer(); 
		timer.Interval = TimeSpan.FromSeconds(1); 
		timer.Tick += OnTimerTick; 
		timer.Start();
	 } 

	void OnTimerTick(object sender, object e) 
	{ 
		txtblk.Text = DateTime.Now.ToString("h:mm:ss tt");     
	} 	
} 
